#!/bin/bash

# Définir les variables
resourceGroup="OCC_ASD_Docker_Karim"
location="westeurope"
vmName="KHDocker"
vmSize="Standard_B1s"
vnetName="KHVNet"
vmSubnetName="KHVMSubnet"
vmPrivateIP="10.0.13.5"     # Adresse IP privée de la machine virtuelle
adminUsername="azureuser"
sshKeyFilePath="$HOME/.ssh/id_rsa"  # Chemin de la clé SSH privée
publicIpName="KHVMPublicIP" # Nom de l'adresse IP publique

# Créer un groupe de ressources
az group create --name $resourceGroup --location $location

# Générer une paire de clés SSH
ssh-keygen -t rsa -b 2048 -C "azureuser@$vmName" -f $sshKeyFilePath -N ""

# Créer un réseau virtuel avec une plage d'adresses spécifiée
az network vnet create --resource-group $resourceGroup --name $vnetName --address-prefix "10.0.0.0/16" --subnet-name $vmSubnetName --subnet-prefix "10.0.13.0/24"

# Créer une adresse IP publique
az network public-ip create --resource-group $resourceGroup --name $publicIpName --sku Standard --allocation-method Static

# Créer une adresse IP privée pour la machine virtuelle avec une association à l'adresse IP publique
az network nic create --resource-group $resourceGroup --name "${vmName}NIC" --vnet-name $vnetName --subnet $vmSubnetName --private-ip-address "$vmPrivateIP" --public-ip-address $publicIpName

# Créer une machine virtuelle Debian 11 en utilisant la clé publique SSH
az vm create \
  --resource-group $resourceGroup \
  --name $vmName \
  --image Debian11 \
  --size $vmSize \
  --nics "${vmName}NIC" \
  --admin-username $adminUsername \
  --ssh-key-value "$(< $sshKeyFilePath.pub)"

# Attendre 1 minute pour que la machine virtuelle soit créée
sleep 60

# Se connecter à la VM en utilisant la clé privée SSH
echo "Connexion à la machine virtuelle..."
ssh -i $sshKeyFilePath $adminUsername@$vmName << EOF

# Installer Docker
sudo apt update
sudo apt install -y docker.io

# Vérifier si l'installation de Docker s'est bien déroulée
if [ $? -ne 0 ]; then
    echo "Erreur: Installation de Docker a échoué."
    exit 1
fi

# Créer un réseau Docker pour Odoo et PostgreSQL
sudo docker network create mynetwork

# Créer un volume Docker pour Odoo
sudo docker volume create odoo-data

# Créer un fichier Dockerfile pour Odoo
cat << DOCKERFILE > Dockerfile
FROM odoo:latest

# Ajouter le volume pour les données Odoo
VOLUME /var/lib/odoo

# Exécuter Odoo
CMD ["odoo"]
DOCKERFILE

# Construire l'image Docker pour Odoo
sudo docker build -t my_odoo .

# Créer un fichier docker-compose.yml pour déployer Odoo et PostgreSQL
cat << DOCKERCOMPOSE > docker-compose.yml
version: '3'

services:
  odoo:
    image: my_odoo
    ports:
      - "8089:8069"
    networks:
      - mynetwork
    volumes:
      - odoo-data:/var/lib/odoo
    depends_on:
      - db
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: YourStrongPostgresPassword$
      POSTGRES_DB: odoo

  db:
    image: postgres:latest
    networks:
      - mynetwork
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: YourStrongPostgresPassword$
      POSTGRES_DB: odoo

networks:
  mynetwork:
    ipam:
      config:
        - subnet: 10.0.13.0/24  # Sous-réseau pour le réseau Docker
volumes:
  odoo-data:
DOCKERCOMPOSE

# Démarrer les conteneurs Odoo et PostgreSQL avec Docker Compose
sudo docker-compose up -d

EOF

# Supprimer la clé publique SSH une fois que le pare-feu sera mis en place
# rm $sshKeyFilePath.pub
