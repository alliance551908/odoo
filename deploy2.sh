#!/bin/bash

# Définir les variables
resourceGroup="OCC_ASD_Docker_Karim"
location="westeurope"
vmName="KHDocker"
vmSize="Standard_B1s"
vnetName="KHDockerVNet"
subnetName="KHDockerSubnet"
addressPrefix="10.0.13.0/24"  # Plage d'adresses IP du réseau virtuel
subnetPrefix="10.0.13.0/28"     # Plage d'adresses IP du sous-réseau

# Créer un groupe de ressources
az group create --name $resourceGroup --location $location

# Créer une machine virtuelle avec Docker préinstallé
az vm create --resource-group $resourceGroup --name $vmName --image Debian11 --size $vmSize --admin-username azureuser --generate-ssh-keys --custom-data cloud-init.txt

# Attendre que la machine virtuelle soit prête avant de continuer
echo "Attente que la machine virtuelle soit prête..."
sleep 60

# Obtenir l'adresse IP de la machine virtuelle
VM_IP=$(az vm show -d -g $resourceGroup -n $vmName --query "publicIps" -o tsv)

# Se connecter à la machine virtuelle
echo "Connexion à la machine virtuelle..."
ssh azureuser@$VM_IP << 'EOF'

# Installer Docker Compose
sudo apt update
sudo apt install -y docker-compose

# Créer un conteneur Nginx
sudo docker run -d --name nginx -p 80:80 nginx

# Créer un répertoire pour le Dockerfile Odoo
mkdir odoo-dockerfile
cd odoo-dockerfile

# Créer le Dockerfile pour Odoo
cat << 'DOCKERFILE' > Dockerfile
FROM odoo:latest

# Ajouter le volume pour les données Odoo
VOLUME /var/lib/odoo

# Définir le réseau Docker pour Odoo
networks:
  - odoo-network

# Exécuter Odoo
CMD ["odoo"]
DOCKERFILE

# Créer un réseau Docker pour Odoo
sudo docker network create odoo-network

EOF
