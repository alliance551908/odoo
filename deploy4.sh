#!/bin/bash

# Définir les variables
resourceGroup="OCC_ASD_Docker_Karim"
location="westeurope"
vmName="KHDocker"
vmSize="Standard_B1s"
vnetName="KHVNet"
vmSubnetName="KHVMSubnet"
fwSubnetName="KHFirewallSubnet"
fwName="KHFirewall"
fwPublicIPName="KHFirewallPublicIP"
keyVaultName="KHMHKeyVault"
secretNamePostgres="PostgresPassword"
secretNameAdmin="AdminPassword"
addressPrefix="10.0.13.0/24"          # Plage d'adresses IP du réseau virtuel
vmSubnetPrefix="10.0.13.0/28"          # Plage d'adresses IP du sous-réseau pour la VM
fwSubnetPrefix="10.0.13.16/28"         # Plage d'adresses IP du sous-réseau pour le pare-feu
vmPrivateIP="10.0.13.5"                # Définir l'adresse IP privée de la machine virtuelle

# Créer un groupe de ressources
az group create --name $resourceGroup --location $location || { echo "Erreur lors de la création du groupe de ressources."; exit 1; }

# Créer un réseau virtuel avec une plage d'adresses spécifiée
az network vnet create --resource-group $resourceGroup --name $vnetName --address-prefix $addressPrefix --subnet-name $vmSubnetName --subnet-prefix $vmSubnetPrefix || { echo "Erreur lors de la création du réseau virtuel."; exit 1; }

# Attendre 30 secondes pour s'assurer que le réseau virtuel est créé
sleep 30

# Créer une adresse IP privée pour la machine virtuelle
az network nic create --resource-group $resourceGroup --name "${vmName}NIC" --vnet-name $vnetName --subnet $vmSubnetName --private-ip-address "$vmPrivateIP" || { echo "Erreur lors de la création de l'adresse IP privée."; exit 1; }

# Créer une machine virtuelle Debian 11
az vm create \
  --resource-group $resourceGroup \
  --name $vmName \
  --image Debian11 \
  --size $vmSize \
  --nics "${vmName}NIC" \
  --admin-username azureuser \
  --generate-ssh-keys || { echo "Erreur lors de la création de la machine virtuelle."; exit 1; }

# Attendre 30 secondes pour que la machine virtuelle soit créée
sleep 30

# Ajouter la VM à son sous-réseau
az network vnet subnet update --resource-group $resourceGroup --vnet-name $vnetName --name $vmSubnetName --add addressPrefixes $vmSubnetPrefix || { echo "Erreur lors de l'ajout de la VM à son sous-réseau."; exit 1; }

# Créer un sous-réseau pour le pare-feu
az network vnet subnet create --resource-group $resourceGroup --vnet-name $vnetName --name $fwSubnetName --address-prefix $fwSubnetPrefix || { echo "Erreur lors de la création du sous-réseau pour le pare-feu."; exit 1; }

# Attendre 30 secondes pour s'assurer que le sous-réseau du pare-feu est créé
sleep 30

# Créer une adresse IP publique pour le pare-feu
az network public-ip create --resource-group $resourceGroup --name $fwPublicIPName --sku Standard --allocation-method Static || { echo "Erreur lors de la création de l'adresse IP publique pour le pare-feu."; exit 1; }

# Récupérer l'adresse IP publique du pare-feu
fwPublicIP=$(az network public-ip show --resource-group $resourceGroup --name $fwPublicIPName --query ipAddress -o tsv) || { echo "Erreur lors de la récupération de l'adresse IP publique du pare-feu."; exit 1; }

# Créer un pare-feu
az network firewall create --resource-group $resourceGroup --name $fwName --location $location || { echo "Erreur lors de la création du pare-feu."; exit 1; }

# Associer la carte réseau du pare-feu à son adresse IP publique
az network firewall ip-config update --resource-group $resourceGroup --name $fwName --public-ip-address $fwPublicIP || { echo "Erreur lors de l'association de la carte réseau du pare-feu à son adresse IP publique."; exit 1; }

# Ajouter le pare-feu à son sous-réseau
az network vnet subnet update --resource-group $resourceGroup --vnet-name $vnetName --name $fwSubnetName --add delegations "[{\"name\": \"ACIFWDelegation\", \"properties\": {\"serviceName\": \"Microsoft.Network/azureFirewalls\"}}]" || { echo "Erreur lors de l'ajout du pare-feu à son sous-réseau."; exit 1; }

# Configurer les règles du pare-feu pour la redirection de port
az network firewall nat-rule collection create \
  --resource-group $resourceGroup \
  --name "RedirectRuleGroup" \
  --firewall-name $fwName \
  --priority 100 || { echo "Erreur lors de la configuration des règles du pare-feu pour la redirection de port."; exit 1; }

az network firewall nat-rule create \
  --resource-group $resourceGroup \
  --collection-name "RedirectRuleGroup" \
  --name "RedirectRule" \
  --firewall-name $fwName \
  --protocols "TCP" \
  --source-addresses "*" \
  --source-ports "*" \
  --destination-addresses $fwPublicIPName \
  --destination-ports 80 \
  --action "DNAT" \
  --priority 100 \
  --translated-address "$vmPrivateIP" \
  --translated-port 8089 || { echo "Erreur lors de la création de la règle NAT pour la redirection de port."; exit 1; }

# Attendre 30 secondes pour s'assurer que la règle de pare-feu est appliquée
sleep 30

# Créer un Azure Key Vault
az keyvault create --name $keyVaultName --resource-group $resourceGroup || { echo "Erreur lors de la création du coffre-fort Azure Key Vault."; exit 1; }

# Demander les mots de passe à l'utilisateur
read -s -p "Entrez le mot de passe pour PostgreSQL : " postgresPassword
echo ""
read -s -p "Entrez le mot de passe pour l'utilisateur admin : " adminPassword
echo ""

# Valider les mots de passe
# Ajouter ici la logique de validation des mots de passe si nécessaire

# Ajouter des secrets au Key Vault
az keyvault secret set --vault-name $keyVaultName --name $secretNamePostgres --value $postgresPassword || { echo "Erreur lors de l'ajout du secret pour PostgreSQL au coffre-fort Azure Key Vault."; exit 1; }
az keyvault secret set --vault-name $keyVaultName --name $secretNameAdmin --value $adminPassword || { echo "Erreur lors de l'ajout du secret pour l'utilisateur admin au coffre-fort Azure Key Vault."; exit 1; }

# Récupérer les mots de passe depuis le Key Vault
postgresPassword=$(az keyvault secret show --name $secretNamePostgres --vault-name $keyVaultName --query value -o tsv) || { echo "Erreur lors de la récupération du mot de passe PostgreSQL depuis le coffre-fort Azure Key Vault."; exit 1; }
adminPassword=$(az keyvault secret show --name $secretNameAdmin --vault-name $keyVaultName --query value -o tsv) || { echo "Erreur lors de la récupération du mot de passe pour l'utilisateur admin depuis le coffre-fort Azure Key Vault."; exit 1; }

# Récupérer l'adresse IP de la VM
VM_IP=$vmPrivateIP

# Se connecter à la VM
echo "Connexion à la machine virtuelle..."
ssh azureuser@$VM_IP << EOF

# Vérifier si l'installation de Docker s'est bien déroulée
if ! command -v docker &> /dev/null
then
    echo "Docker n'est pas installé. Installation en cours..."
    sudo apt update && sudo apt install -y docker.io
    if [ $? -ne 0 ]; then
        echo "Erreur: Installation de Docker a échoué."
        exit 1
    fi
fi

# Créer un réseau Docker pour Odoo et PostgreSQL
sudo docker network create mynetwork

# Créer un volume Docker pour Odoo
sudo docker volume create odoo-data

# Créer un fichier Dockerfile pour Odoo
cat << DOCKERFILE > Dockerfile
FROM odoo:latest

# Ajouter le volume pour les données Odoo
VOLUME /var/lib/odoo

# Exécuter Odoo
CMD ["odoo"]
DOCKERFILE

# Construire l'image Docker pour Odoo
sudo docker build -t my_odoo .

# Créer un fichier docker-compose.yml pour déployer Odoo et PostgreSQL
cat << DOCKERCOMPOSE > docker-compose.yml
version: '3'

services:
  odoo:
    image: my_odoo
    ports:
      - "8089:8069"
    networks:
      - mynetwork:
          ipv4_address: 10.0.13.6  # Adresse IP spécifique pour Odoo
    volumes:
      - odoo-data:/var/lib/odoo
    depends_on:
      - db
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: $postgresPassword
      POSTGRES_DB: odoo

  db:
    image: postgres:latest
    networks:
      - mynetwork:
          ipv4_address: 10.0.13.7  # Adresse IP spécifique pour la base de données
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: $postgresPassword
      POSTGRES_DB: odoo

networks:
  mynetwork:
    ipam:
      config:
        - subnet: 10.0.13.0/24  # Sous-réseau pour le réseau Docker
volumes:
  odoo-data:
DOCKERCOMPOSE

# Démarrer les conteneurs Odoo et PostgreSQL avec Docker Compose
sudo docker-compose up -d

EOF
