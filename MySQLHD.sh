#!/bin/bash

# Variables
resource_group="OCC_ASD_Karim_MySQL"
location="francecentral"
virtual_network_name="Karim_VNet"
subnet1_name="MySQL_Subnet1"
subnet2_name="MySQL_Subnet2"
master_server_name="wp-db-server-master"
master_db_user="master_user"
master_db_password="master_password"  # Remplacez par le mot de passe du compte maître
replica_name="wp-db-server-slave"   # Nouveau nom du réplica pour le serveur MySQL maître

# master_server_address="wp-db-server-master-1.mysql.database.azure.com"  # Adresse du serveur maître fournie par Azure
# Ajout d'un suffixe numérique au nom de serveur
# master_server_name="$master_server_name-1"
# replica_name="$replica_name-1"

# Créer le premier sous-réseau (/28)
subnet1_address_prefix="10.0.13.32/28"
echo "Création du premier sous-réseau avec l'adresse IP $subnet1_address_prefix..."
az network vnet subnet create \
    --resource-group $resource_group \
    --vnet-name $virtual_network_name \
    --name $subnet1_name \
    --address-prefix $subnet1_address_prefix

# Récupérer l'ID du premier sous-réseau
subnet1_id=$(az network vnet subnet show --resource-group $resource_group --vnet-name $virtual_network_name --name $subnet1_name --query id -o tsv)

# Créer le deuxième sous-réseau (/28)
subnet2_address_prefix="10.0.13.48/28"
echo "Création du deuxième sous-réseau avec l'adresse IP $subnet2_address_prefix..."
az network vnet subnet create \
    --resource-group $resource_group \
    --vnet-name $virtual_network_name \
    --name $subnet2_name \
    --address-prefix $subnet2_address_prefix

# Récupérer l'ID du deuxième sous-réseau
subnet2_id=$(az network vnet subnet show --resource-group $resource_group --vnet-name $virtual_network_name --name $subnet2_name --query id -o tsv)

# Fonction pour créer un serveur MySQL flexible avec haute disponibilité SameZone
create_flexible_mysql_server() {
    local server_name="$1"
    local admin_user="$2"
    local admin_password="$3"
    local subnet_id="$4"
    local high_availability="$5"  # Niveau de haute disponibilité
    local sku_name="$6"           # SKU du serveur MySQL flexible
    local tier="$7"               # Tier du serveur MySQL flexible
    local location="$8"           # Location du serveur MySQL flexible
    local resource_group="$9"     # Groupe de ressources du serveur MySQL flexible

    az mysql flexible-server create --yes  \
        --resource-group $resource_group \
        --name $server_name \
        --location $location \
        --admin-user $admin_user \
        --admin-password $admin_password \
        --sku-name $sku_name \
        --tier $tier \
        --subnet $subnet_id \
        --high-availability $high_availability  # Spécifier le niveau de haute disponibilité
}

# Fonction pour créer un réplica pour un serveur MySQL flexible
create_mysql_replica() {
    local source_server="$1"
    local replica_name="$2"
    local resource_group="$3"
    local subnet_id="$4"
    local private_dns_zone="$5"  # Ajoutez le paramètre pour la zone DNS privée

    az mysql flexible-server replica create \
        --replica-name $replica_name \
        --resource-group $resource_group \
        --source-server $source_server \
        --subnet $subnet_id \
        --private-dns-zone $private_dns_zone  # Spécifiez la zone DNS privée
}

# Créer le serveur MySQL maître avec haute disponibilité SameZone
echo "Création du serveur MySQL maître avec haute disponibilité SameZone..."
create_flexible_mysql_server $master_server_name $master_db_user $master_db_password $subnet1_id "SameZone" "Standard_D2ds_v4" "GeneralPurpose" "francecentral" $resource_group

# Créer le réplica pour le serveur MySQL maître
echo "Création du réplica pour le serveur MySQL maître..."
create_mysql_replica $master_server_name $replica_name $resource_group $subnet2_id wp-db-server-master.private.mysql.database.azure.com

echo "Configuration du serveur maître et du réplica terminée."