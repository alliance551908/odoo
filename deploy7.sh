#!/bin/bash

# Définir les variables
resourceGroup="OCC_ASD_Docker_Karim"
location="westeurope"
vmName="KHDocker"
vmSize="Standard_B1s"
vnetName="KHVNet"
vmSubnetName="KHVMSubnet"
vmPrivateIP="10.0.13.5"     # Adresse IP privée de la machine virtuelle
adminUsername="azureuser"
adminPassword="YourStrongPassword$"  # Remplacez par le mot de passe souhaité

# Créer un groupe de ressources
az group create --name $resourceGroup --location $location

# Créer un réseau virtuel avec une plage d'adresses spécifiée
az network vnet create --resource-group $resourceGroup --name $vnetName --address-prefix "10.0.0.0/16" --subnet-name $vmSubnetName --subnet-prefix "10.0.13.0/24"

# Créer une adresse IP privée pour la machine virtuelle
az network nic create --resource-group $resourceGroup --name "${vmName}NIC" --vnet-name $vnetName --subnet $vmSubnetName --private-ip-address "$vmPrivateIP"

# Créer une machine virtuelle Debian 11 et récupérer son adresse IP
VM_IP=$(az vm create \
  --resource-group $resourceGroup \
  --name $vmName \
  --image Debian11 \
  --size $vmSize \
  --nics "${vmName}NIC" \
  --admin-username $adminUsername \
  --admin-password $adminPassword \
  --authentication-type password \
  --generate-ssh-keys \
  --query "publicIpAddress" -o tsv)

# Attendre 1 minute pour que la machine virtuelle soit créée
sleep 60

# Se connecter à la VM
echo "Connexion à la machine virtuelle..."
ssh $adminUsername@$VM_IP << EOF
# Commandes à exécuter sur la machine virtuelle
EOF
