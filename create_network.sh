#!/bin/bash

# Configuration
resource_group="OCC_ASD_Karim"
vnet_name="Vnet_OCC_ASD_Karim"
vnet_cidr="10.0.13.0/24"
subnet_name="Subnet_Vnet_OCC_ASD_Karim"
subnet_cidr="10.0.13.0/28"

# Création du sous-réseau principal
az network vnet subnet create \
    --name $subnet_name \
    --resource-group $resource_group \
    --vnet-name $vnet_name \
    --address-prefix $subnet_cidr

# Création des sous-réseaux supplémentaires
for ((i=1; i<=15; i++))
do
    az network vnet subnet create \
        --name "$subnet_name-$i" \
        --resource-group $resource_group \
        --vnet-name $vnet_name \
        --address-prefix "10.0.13.$((i*16))/28"
done