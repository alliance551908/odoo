#!/bin/bash

# Variables
resourceGroupName="OCC_ASD_Karim_MySQL"
vmName1="Karim_WP1"
vmName2="Karim_WP2"
location="francecentral"
adminUsername="azureuser"
vnetName="Karim_VNet"
subnetPrefix="10.0.13.0/24"
subnetPrefixVM1="10.0.13.0/28"
subnetPrefixVM2="10.0.13.16/28"
defaultSiteName1="example1"
defaultSiteName2="example2"

# Créer un groupe de ressources
az group create --name $resourceGroupName --location $location

# Créer un réseau virtuel avec un sous-réseau pour les deux machines virtuelles
az network vnet create \
    --name $vnetName \
    --resource-group $resourceGroupName \
    --location $location \
    --address-prefixes $subnetPrefix \
    --subnet-name WP_Subnet1 \
    --subnet-prefix $subnetPrefixVM1

# Créer le deuxième sous-réseau dans le même VNet pour la deuxième machine virtuelle
az network vnet subnet create \
    --name WP_Subnet2 \
    --resource-group $resourceGroupName \
    --vnet-name $vnetName \
    --address-prefix $subnetPrefixVM2

# Créer la première machine virtuelle Debian 11 dans le sous-réseau VM1
az vm create \
    --resource-group $resourceGroupName \
    --name $vmName1 \
    --image Debian11 \
    --admin-username $adminUsername \
    --generate-ssh-keys \
    --vnet-name $vnetName \
    --subnet WP_Subnet1 \
    --location $location \
    --verbose

# Récupérer l'adresse IP publique de la première machine virtuelle
publicIPAddress1=$(az vm show -d -g $resourceGroupName -n $vmName1 --query publicIps -o tsv)

# Établir la connexion SSH avec la première machine virtuelle en acceptant automatiquement le fingerprint
ssh -o StrictHostKeyChecking=no $adminUsername@$publicIPAddress1

# Créer la deuxième machine virtuelle Debian 11 dans le sous-réseau VM2
az vm create \
    --resource-group $resourceGroupName \
    --name $vmName2 \
    --image Debian11 \
    --admin-username $adminUsername \
    --generate-ssh-keys \
    --vnet-name $vnetName \
    --subnet WP_Subnet2 \
    --location $location \
    --verbose

# Récupérer l'adresse IP publique de la deuxième machine virtuelle
publicIPAddress2=$(az vm show -d -g $resourceGroupName -n $vmName2 --query publicIps -o tsv)

# Établir la connexion SSH avec la deuxième machine virtuelle en acceptant automatiquement le fingerprint
ssh -o StrictHostKeyChecking=no $adminUsername@$publicIPAddress2

# Première machine virtuelle
az network nsg rule create \
    --name AllowHTTP \
    --resource-group $resourceGroupName \
    --nsg-name ${vmName1}NSG \
    --priority 1001 \
    --protocol Tcp \
    --direction Inbound \
    --source-address-prefixes '*' \
    --source-port-ranges '*' \
    --destination-address-prefixes '*' \
    --destination-port-ranges 80 \
    --access Allow

# Deuxième machine virtuelle
az network nsg rule create \
    --name AllowHTTP \
    --resource-group $resourceGroupName \
    --nsg-name ${vmName2}NSG \
    --priority 1001 \
    --protocol Tcp \
    --direction Inbound \
    --source-address-prefixes '*' \
    --source-port-ranges '*' \
    --destination-address-prefixes '*' \
    --destination-port-ranges 80 \
    --access Allow

# Fonction pour installer WordPress sur une machine virtuelle
install_wordpress() {
    local ip_address="$1"
    ssh $adminUsername@$ip_address > /dev/null 2>&1 << EOF
        # Mettre à jour et installer les dépendances nécessaires
        sudo apt update > /dev/null 2>&1
        sudo apt install -y apache2 php libapache2-mod-php php-mysql curl php-cli php-mbstring php-gd php-xml unzip > /dev/null 2>&1

        # Télécharger et installer WordPress pour le premier site
        cd /var/www/html
        sudo wget https://wordpress.org/latest.tar.gz > /dev/null 2>&1
        sudo tar -xzvf latest.tar.gz > /dev/null 2>&1
        sudo mv wordpress "$defaultSiteName1" > /dev/null 2>&1
        cd "$defaultSiteName1"
        sudo cp wp-config-sample.php wp-config.php > /dev/null 2>&1
        sudo sed -i "s/database_name_here/wordpress/g" wp-config.php > /dev/null 2>&1
        sudo sed -i "s/username_here/root/g" wp-config.php > /dev/null 2>&1
        sudo sed -i "s/password_here/password/g" wp-config.php > /dev/null 2>&1
        sudo sed -i "s/localhost/127.0.0.1/g" wp-config.php > /dev/null 2>&1
        sudo chown -R www-data:www-data /var/www/html/"$defaultSiteName1" > /dev/null 2>&1
        sudo chmod -R 755 /var/www/html/"$defaultSiteName1" > /dev/null 2>&1

        # Télécharger et installer WordPress pour le deuxième site
        cd /var/www/html
        sudo wget https://wordpress.org/latest.tar.gz > /dev/null 2>&1
        sudo tar -xzvf latest.tar.gz > /dev/null 2>&1
        sudo mv wordpress "$defaultSiteName2" > /dev/null 2>&1
        cd "$defaultSiteName2"
        sudo cp wp-config-sample.php wp-config.php > /dev/null 2>&1
        sudo sed -i "s/database_name_here/wordpress2/g" wp-config.php > /dev/null 2>&1
        sudo sed -i "s/username_here/root/g" wp-config.php > /dev/null 2>&1
        sudo sed -i "s/password_here/password/g" wp-config.php > /dev/null 2>&1
        sudo sed -i "s/localhost/127.0.0.1/g" wp-config.php > /dev/null 2>&1
        sudo chown -R www-data:www-data /var/www/html/"$defaultSiteName2" > /dev/null 2>&1
        sudo chmod -R 755 /var/www/html/"$defaultSiteName2" > /dev/null 2>&1

        # Configurer Apache pour le premier site
        sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/"$defaultSiteName1".conf > /dev/null 2>&1
        sudo sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/$defaultSiteName1/g" /etc/apache2/sites-available/"$defaultSiteName1".conf > /dev/null 2>&1
        sudo sed -i "s/<VirtualHost \*:80>/<VirtualHost *:80>\n  ServerName ${defaultSiteName1}.lan/g" /etc/apache2/sites-available/"$defaultSiteName1".conf > /dev/null 2>&1
        sudo a2ensite "$defaultSiteName1".conf > /dev/null 2>&1

        # Configurer Apache pour le deuxième site
        sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/"$defaultSiteName2".conf > /dev/null 2>&1
        sudo sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/html\/$defaultSiteName2/g" /etc/apache2/sites-available/"$defaultSiteName2".conf > /dev/null 2>&1
        sudo sed -i "s/<VirtualHost \*:80>/<VirtualHost *:80>\n  ServerName ${defaultSiteName2}.lan/g" /etc/apache2/sites-available/"$defaultSiteName2".conf > /dev/null 2>&1
        sudo a2ensite "$defaultSiteName2".conf > /dev/null 2>&1

        # Redémarrer Apache
        sudo systemctl restart apache2 > /dev/null 2>&1

EOF
    if [ $? -eq 0 ]; then
        echo "WordPress a été installé avec succès sur la machine virtuelle ($ip_address)."
    else
        echo "Erreur lors de l'installation de WordPress sur la machine virtuelle ($ip_address)."
    fi
}

# Installer WordPress sur la première machine virtuelle
echo "Installation de WordPress sur la première machine virtuelle ($publicIPAddress1)..."
install_wordpress $publicIPAddress1

# Installer WordPress sur la deuxième machine virtuelle
echo "Installation de WordPress sur la deuxième machine virtuelle ($publicIPAddress2)..."
install_wordpress $publicIPAddress2