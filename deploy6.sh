#!/bin/bash

# Définir les variables
resourceGroup="OCC_ASD_Docker_Karim"
location="westeurope"
vmName="KHDocker"
vmSize="Standard_B1s"
vnetName="KHVNet"
vmSubnetName="KHVMSubnet"
vmPrivateIP="10.0.13.5"     # Adresse IP privée de la machine virtuelle
adminUsername="azureuser"
adminPassword="YourStrongPassword$"  # Remplacez par le mot de passe souhaité

# Créer un groupe de ressources
az group create --name $resourceGroup --location $location

# Créer un réseau virtuel avec une plage d'adresses spécifiée
az network vnet create --resource-group $resourceGroup --name $vnetName --address-prefix "10.0.0.0/16" --subnet-name $vmSubnetName --subnet-prefix "10.0.13.0/24"

# Créer une adresse IP privée pour la machine virtuelle
az network nic create --resource-group $resourceGroup --name "${vmName}NIC" --vnet-name $vnetName --subnet $vmSubnetName --private-ip-address "$vmPrivateIP"

# Créer une machine virtuelle Debian 11
az vm create \
  --resource-group $resourceGroup \
  --name $vmName \
  --image Debian11 \
  --size $vmSize \
  --nics "${vmName}NIC" \
  --admin-username $adminUsername \
  --admin-password $adminPassword \
  --authentication-type password \
  --generate-ssh-keys  # Ignoré lors de l'utilisation du mot de passe pour l'authentification SSH

# Attendre 1 minute pour que la machine virtuelle soit créée
sleep 60

# Récupérer l'adresse IP de la machine virtuelle
VM_IP=$(az vm show -d -g $resourceGroup -n $vmName --query "publicIps" -o tsv)

# Se connecter à la VM
echo "Connexion à la machine virtuelle..."
ssh $adminUsername@$VM_IP << EOF

# Installer Docker
sudo apt update
sudo apt install -y docker.io

# Vérifier si l'installation de Docker s'est bien déroulée
if [ $? -ne 0 ]; then
    echo "Erreur: Installation de Docker a échoué."
    exit 1
fi

# Créer un réseau Docker pour Odoo et PostgreSQL
sudo docker network create mynetwork

# Créer un volume Docker pour Odoo
sudo docker volume create odoo-data

# Créer un fichier Dockerfile pour Odoo
cat << DOCKERFILE > Dockerfile
FROM odoo:latest

# Ajouter le volume pour les données Odoo
VOLUME /var/lib/odoo

# Exécuter Odoo
CMD ["odoo"]
DOCKERFILE

# Construire l'image Docker pour Odoo
sudo docker build -t my_odoo .

# Créer un fichier docker-compose.yml pour déployer Odoo et PostgreSQL
cat << DOCKERCOMPOSE > docker-compose.yml
version: '3'

services:
  odoo:
    image: my_odoo
    ports:
      - "8089:8069"
    networks:
      - mynetwork
    volumes:
      - odoo-data:/var/lib/odoo
    depends_on:
      - db
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: YourStrongPostgresPassword$
      POSTGRES_DB: odoo

  db:
    image: postgres:latest
    networks:
      - mynetwork
    environment:
      POSTGRES_USER: odoo
      POSTGRES_PASSWORD: YourStrongPostgresPassword$
      POSTGRES_DB: odoo

networks:
  mynetwork:
    ipam:
      config:
        - subnet: 10.0.13.0/24  # Sous-réseau pour le réseau Docker
volumes:
  odoo-data:
DOCKERCOMPOSE

# Démarrer les conteneurs Odoo et PostgreSQL avec Docker Compose
sudo docker-compose up -d

EOF
