#!/bin/bash

# Variables
resource_group="ResourceGroupABC"
location="francecentral"
virtual_network_name="myVNet"
subnet1_name="Subnet3"
subnet2_name="Subnet4"
master_server_name="wp-db-server-master"
slave_server_name="wp-db-server-slave"
master_db_user="master_user"
master_db_password="master_password"  # Remplacez par le mot de passe du compte maître
slave_db_user="slave_user"
slave_db_password="slave_password"   # Remplacez par le mot de passe du compte esclave
master_server_address="wp-db-server-master-1.mysql.database.azure.com"  # Adresse du serveur maître fournie par Azure

# Ajout d'un suffixe numérique au nom de serveur
master_server_name="$master_server_name-1"
slave_server_name="$slave_server_name-1"

# Créer le premier sous-réseau (/28)
subnet1_address_prefix="10.0.13.32/28"
echo "Création du premier sous-réseau avec l'adresse IP $subnet1_address_prefix..."
az network vnet subnet create \
    --resource-group $resource_group \
    --vnet-name $virtual_network_name \
    --name $subnet1_name \
    --address-prefix $subnet1_address_prefix

# Récupérer l'ID du premier sous-réseau
subnet1_id=$(az network vnet subnet show --resource-group $resource_group --vnet-name $virtual_network_name --name $subnet1_name --query id -o tsv)

# Créer le deuxième sous-réseau (/28)
subnet2_address_prefix="10.0.13.48/28"
echo "Création du deuxième sous-réseau avec l'adresse IP $subnet2_address_prefix..."
az network vnet subnet create \
    --resource-group $resource_group \
    --vnet-name $virtual_network_name \
    --name $subnet2_name \
    --address-prefix $subnet2_address_prefix

# Récupérer l'ID du deuxième sous-réseau
subnet2_id=$(az network vnet subnet show --resource-group $resource_group --vnet-name $virtual_network_name --name $subnet2_name --query id -o tsv)

# Fonction pour créer un serveur MySQL flexible
create_flexible_mysql_server() {
    local server_name="$1"
    local admin_user="$2"
    local admin_password="$3"
    local subnet_id="$4"
    az mysql flexible-server create --yes  \
        --resource-group $resource_group \
        --name $server_name \
        --location $location \
        --admin-user $admin_user \
        --admin-password $admin_password \
        --sku-name Standard_B1s \
        --tier 'Burstable' \
        --subnet $subnet_id
}

# Créer le serveur MySQL maître
echo "Création du serveur MySQL maître..."
create_flexible_mysql_server $master_server_name $master_db_user $master_db_password $subnet1_id

# Créer le serveur MySQL esclave
echo "Création du serveur MySQL esclave..."
create_flexible_mysql_server $slave_server_name $slave_db_user $slave_db_password $subnet2_id

echo "Création des serveurs MySQL terminée"