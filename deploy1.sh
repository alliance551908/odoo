#!/bin/bash

# Définir les variables
resourceGroup="OCC_ASD_Docker_Karim"
location="westeurope"
vmName="KHDocker"
vmSize="Standard_B1s"
vnetName="KHDockerVNet"
subnetName="KHDockerSubnet"
addressPrefix="10.0.13.0/24"  # Plage d'adresses IP du réseau virtuel
subnetPrefix="10.0.13.0/28"     # Plage d'adresses IP du sous-réseau
keyVaultName="KHMSKeyVault"
secretNamePostgres="postgres-password"
secretNameAdmin="admin-password"
sshKeyName="ssh-docker-private-key"

# Créer un groupe de ressources
az group create --name $resourceGroup --location $location

# Créer une nouvelle paire de clés SSH
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa -N ""

# Ajouter la clé publique au groupe de ressources Azure
az network vnet create --resource-group $resourceGroup --name $vnetName
az vm create --resource-group $resourceGroup --name $vmName --image Debian

# Créer un Azure Key Vault
az keyvault create --name $keyVaultName --resource-group $resourceGroup

# Demander les mots de passe à l'utilisateur
read -s -p "Entrez le mot de passe pour PostgreSQL : " postgresPassword
echo ""
read -s -p "Entrez le mot de passe pour l'utilisateur admin : " adminPassword
echo ""

# Ajouter des secrets au Key Vault
az keyvault secret set --vault-name $keyVaultName --name $secretNamePostgres --value $postgresPassword
az keyvault secret set --vault-name $keyVaultName --name $secretNameAdmin --value $adminPassword

# Adresse IP de la machine virtuelle
VM_IP=$(az vm show -d -g $resourceGroup -n $vmName --query "publicIps" -o tsv)

# Commandes à exécuter sur la machine distante
commands=$(cat << 'SSHCOMMANDS'
sudo apt update
sudo apt install -y docker.io

# Exécuter un conteneur Odoo
sudo docker run -d --name odoo -p 80:8069 --network host -v odoo_data:/var/lib/odoo --restart always odoo:latest

# Exécuter un conteneur PostgreSQL
sudo docker run -d --name postgres -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=$(az keyvault secret show --vault-name $keyVaultName --name $secretNamePostgres --query "value" -o tsv) --restart always postgres:latest

# Vérification du déploiement
sudo docker ps
SSHCOMMANDS
)

# Copier la clé SSH sur la machine distante
echo "$(cat ~/.ssh/id_rsa.pub)" | ssh azureuser@$VM_IP 'cat >> ~/.ssh/authorized_keys'

# Connexion SSH à la machine distante
ssh azureuser@$VM_IP
